/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "./node_modules/tw-elements/dist/js/**/*.js",
  ],
  theme: {
    extend: {
      fontFamily: {
        "lexend-deca": ["Lexend Deca", "sans-serif"],
        "pop-pins": ["font-family", "Poppins", "sans-serif"],
      },
      colors: {
        primary: {
          500: "#114D27 ",
          400: "#1A776F",
          300: "#00B909",
          200: "#37E849",
          100: "#49C759",
        },
        secondary: {
          200: "#0D4AFD",
          100: "#A6C1BF",
        },
        multiColorBtn: {
          200: "#93DF93",
          100: "#21C2C1",
        },
        multiColorCard: {
          200: "#93DF93",
          100: "#21C2C1",
        },
        cardBg: "#FDFDFD",
        bgLayout: "#F5F6FE",
        textPrimary: "#131313",
        textSecondary: "#7B7B7B",
        danger: "#F15A28",
      },
    },
  },
  plugins: [require("tw-elements/dist/plugin")],
};
