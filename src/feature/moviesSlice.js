import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getMovies = createAsyncThunk("movies/getMovies", async () => {
  const response = await axios.get(
    "https://api.themoviedb.org/3/movie/upcoming?api_key=2fccde01a371b106b09a241d6d1d5b49"
  );
  return response.data.results;
});
export const getMovie = createAsyncThunk("movies/getMovie", async (id) => {
  const response = await axios.get(
    `https://api.themoviedb.org/3/movie/${id}?api_key=2fccde01a371b106b09a241d6d1d5b49`
  );
  return response.data;
});
export const getGender = createAsyncThunk("movies/getGender", async () => {
  const response = await axios.get(
    "https://api.themoviedb.org/3/genre/movie/list?api_key=2fccde01a371b106b09a241d6d1d5b49"
  );
  return response.data.genres;
});

const moviesEntity = createEntityAdapter({
  //selectId: (movies) => movies.id,
  //sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const moviesSlice = createSlice({
  name: "movies",
  initialState: moviesEntity.getInitialState({
    isLoading: false,
    isError: null,
  }),
  reducers: {},
  extraReducers: {
    [getMovies.pending]: (state) => {
      state.isLoading = true;
    },
    [getMovies.fulfilled]: (state, action) => {
      state.isLoading = false;
      moviesEntity.setAll(state, action.payload);
    },
    [getMovies.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [getGender.pending]: (state) => {
      state.isLoading = true;
    },
    [getGender.fulfilled]: (state, action) => {
      state.isLoading = false;
      moviesEntity.setAll(state, action.payload);
    },
    [getGender.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [getMovie.pending]: (state) => {
      state.isLoading = true;
    },
    [getMovie.fulfilled]: (state, action) => {
      state.isLoading = false;
      moviesEntity.setOne(state, action.payload);
    },
    [getMovie.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
  },
});

export const moviesSelectors = moviesEntity.getSelectors(
  (state) => state.movies
);
export default moviesSlice.reducer;
