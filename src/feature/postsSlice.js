import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getPosts = createAsyncThunk("posts/getPosts", async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/comments?_limit=10"
  );
  return response.data;
});
export const deletePosts = createAsyncThunk("posts/deletePost", async (id) => {
  await axios.delete(`https://jsonplaceholder.typicode.com/comments/${id}`);
  return id;
});
export const updatePosts = createAsyncThunk(
  "posts/updatePost",
  async ({ id, newObj }) => {
    await axios.patch(`https://jsonplaceholder.typicode.com/comments/${id}`, {
      newObj,
    });
    return { id, changes: newObj };
  }
);

const postsEntity = createEntityAdapter({
  selectId: (posts) => posts.id,
  //sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const postsSlice = createSlice({
  name: "posts",
  initialState: postsEntity.getInitialState({
    isLoading: false,
    isError: null,
  }),
  reducers: {},
  extraReducers: {
    [getPosts.pending]: (state) => {
      state.isLoading = true;
    },
    [getPosts.fulfilled]: (state, action) => {
      state.isLoading = false;
      postsEntity.setAll(state, action.payload);
    },
    [getPosts.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [deletePosts.pending]: (state) => {
      state.isLoading = true;
    },
    [deletePosts.fulfilled]: (state, { payload: id }) => {
      state.isLoading = false;
      postsEntity.removeOne(state, id);
    },
    [deletePosts.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
    [updatePosts.pending]: (state) => {
      state.isLoading = true;
    },
    [updatePosts.fulfilled]: (state, action) => {
      state.isLoading = false;
      postsEntity.updateOne(state, {
        id: action.payload.id,
        changes: action.payload.changes,
      });
    },
    [updatePosts.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = action.error;
    },
  },
});

export const postsSelectors = postsEntity.getSelectors((state) => state.posts);
export default postsSlice.reducer;
