import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
} from "@reduxjs/toolkit";
import axios from "axios";

export const getUsers = createAsyncThunk("users/getUsers", async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/posts"
  );
  return response.data;
});
export const getUser = createAsyncThunk("users/getUser", async (id) => {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/posts/${id}`
  );
  return response.data;
});
export const saveUsers = createAsyncThunk(
  "users/saveUsers",
  async ({ title, body }) => {
    const response = await axios.post(
      "https://jsonplaceholder.typicode.com/users",
      { title, body }
    );
    return response.data;
  }
);
const usersEntity = createEntityAdapter({
  isLoading: false,
  selectId: (posts) => posts.id,
  sortComparer: (a, b) => a.title.localeCompare(b.title),
});

const usersSlice = createSlice({
  name: "users",
  initialState: usersEntity.getInitialState,
  extraReducers: {
    [getUsers.fulfilled]: (state, action) => {
      usersEntity.setAll(state, action.payload);
    },
    [getUser.fulfilled]: (state, action) => {
      usersEntity.setOne(state, action.payload);
    },
    [saveUsers.fulfilled]: (state, action) => {
      usersEntity.addOne(state, action.payload);
    },
  },
});

export const usersSelectors = usersEntity.getSelectors((state) => state.users);
export default usersSlice.reducer;
