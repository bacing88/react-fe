import { createSlice } from "@reduxjs/toolkit";

const identitasSlice = createSlice({
  name: "identitas",
  initialState: {
    value: 1,
    hidden: null,
  },
  reducers: {
    update: (state, action) => {
      state.value = action.payload;
    },
    updateHidden: (state, action) => {
      state.hidden = action.payload;
    },
  },
});

export const { update, updateHidden } = identitasSlice.actions;
export default identitasSlice.reducer;
