import React from "react";

function noMacht() {
  return (
    <div className="flex justify-center items-center text-3xl text-danger">
      Router Not Found
    </div>
  );
}

export default noMacht;
