import React from "react";
import { BellIcon, HeartIcon } from "@heroicons/react/24/solid";

function NavBarReal() {
  return (
    <div className="card-primary font-pop-pins">
      <div className="flex justify-between items-center px-3 py-1.5">
        <p className="uppercase font-semibold select-none ">
          Real Estate Finder
        </p>
        <div className="flex">
          <button className="">
            <span className="flex text-[#42505C]">
              <BellIcon className="h-5 w-5" />
              <div className="relative inline-flex rounded-full h-2 w-2 top-0 right-2 bg-danger"></div>
            </span>
          </button>
          <button>
            <HeartIcon className="h-5 w-5 text-[#42505C]" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default NavBarReal;
