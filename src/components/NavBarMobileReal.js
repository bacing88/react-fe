import React from "react";
import {
  ChatBubbleOvalLeftEllipsisIcon,
  MagnifyingGlassIcon,
  IdentificationIcon,
  ArrowPathIcon,
  UserIcon,
} from "@heroicons/react/24/outline";

function NavBarMobileReal() {
  return (
    <div className="block sm:hidden mt-1">
      <div className="fixed bottom-0 w-full p-5 px-6 flex items-center justify-between bg-white text-white cursor-pointer">
        <div className="flex flex-col items-center justify-center text-[#79828B] cursor-pointer">
          <MagnifyingGlassIcon className="w-7 h-7 stroke-2 p-1 text-white bg-[#79828B] rounded-full" />
          <p className="text-sm mt-1">Search</p>
        </div>
        <div className="flex flex-col items-center justify-center text-[#79828B] cursor-pointer">
          <ChatBubbleOvalLeftEllipsisIcon className="w-7 h-7 stroke-2 p-1 text-white bg-[#79828B] rounded-full" />
          <p className="text-sm mt-1">Chat</p>
        </div>
        <div className="flex flex-col items-center justify-center text-[#79828B] cursor-pointer">
          <IdentificationIcon className="w-7 h-7 stroke-1 p-1 text-white bg-[#79828B] rounded-full" />
          <p className="text-sm mt-1">Category</p>
        </div>
        <div className="flex flex-col items-center justify-center text-[#79828B] cursor-pointer">
          <ArrowPathIcon className="w-7 h-7 stroke-2 p-1 text-white bg-[#79828B] rounded-full" />
          <p className="text-sm mt-1">History</p>
        </div>
        <div className="flex flex-col items-center justify-center text-[#79828B] cursor-pointer">
          <UserIcon className="w-7 h-7 stroke-2 p-1 text-white bg-[#79828B] rounded-full" />
          <p className="text-sm mt-1">Profile</p>
        </div>
      </div>
    </div>
  );
}

export default NavBarMobileReal;
