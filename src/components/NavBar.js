import React from "react";
import { BellAlertIcon, ChevronDownIcon } from "@heroicons/react/24/solid";

function NavBar() {
  return (
    <div className="card-primary">
      <div className="flex justify-between items-center px-3 py-1.5">
        <div className="flex items-center space-x-2">
          <img
            className="inline-block h-8 w-8 rounded-full"
            src="https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt="user"
          />
          <p className="text-sm text-textSecondary font-medium">Welcome,</p>
          <p className="text-sm text-primary-400 font-medium">Username</p>
        </div>
        <div className="flex">
          <button className="flex items-center space-x-2 text-textSecondary">
            <BellAlertIcon className="h-5 w-5" />
            <span className="ml-3 text-sm font-medium"> Notivication </span>
          </button>
          <button className="flex items-center space-x-2 text-textSecondary">
            <span className="ml-3 text-sm font-medium"> Profile </span>
            <ChevronDownIcon className="h-5 w-5" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default NavBar;
