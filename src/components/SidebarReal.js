import React from "react";
import Logo from "../image/text-logo.png";
import { NavLink } from "react-router-dom";

export default function SidebarReal() {
  return (
    <aside className="w-52 fixed left-0 top-0 hidden md:block bg-[#FCFCFC] drop-shadow-sm font-pop-pins">
      <div className="flex flex-col h-screen justify-between">
        <div className="px-2 py-5">
          <div className="px-10 -mb-2">
            <img src={Logo} width="250" height="200" alt="logo" />
          </div>

          <nav className="flex flex-col mt-6 space-y-1">
            <NavLink
              to="#"
              className="flex items-center px-4 py-2 font-light text-white bg-[#79828B] rounded-md hover:bg-[#42505C]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-current"
              >
                <path
                  className="a"
                  d="M14.677,12.911a8.155,8.155,0,1,0-1.767,1.768L18.232,20,20,18.234l-5.323-5.323Zm-6.552.841A5.625,5.625,0,1,1,13.75,8.127a5.631,5.631,0,0,1-5.625,5.625Z"
                  transform="translate(0 -0.002)"
                />
              </svg>
              <span className="ml-3 text-sm font-medium"> SEARCH </span>
            </NavLink>
            <NavLink
              to="#"
              className="flex items-center px-4 py-2 font-light text-white bg-[#79828B] rounded-md hover:bg-[#42505C]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-current"
              >
                <g transform="translate(0 -0.003)">
                  <path
                    className="a"
                    d="M120.834,329.539a.625.625,0,1,1-.884,0,.625.625,0,0,1,.884,0"
                    transform="translate(-114.603 -315.146)"
                  />
                  <path
                    className="a"
                    d="M180.717,329.539a.625.625,0,1,1-.884,0,.625.625,0,0,1,.884,0"
                    transform="translate(-171.902 -315.146)"
                  />
                  <path
                    className="a"
                    d="M240.6,329.539a.625.625,0,1,1-.884,0,.625.625,0,0,1,.884,0"
                    transform="translate(-229.201 -315.146)"
                  />
                  <path
                    className="a"
                    d="M18.663,11.533a.641.641,0,0,1-.019-.859A6.109,6.109,0,0,0,20,6.878C20,3.087,16.355,0,11.875,0S3.75,3.087,3.75,6.878c0,.152.025.3.038.446A6.708,6.708,0,0,0,0,13.128a6.109,6.109,0,0,0,1.355,3.8.641.641,0,0,1-.019.859L.183,18.936A.625.625,0,0,0,.625,20h7.5c4.231,0,7.713-2.751,8.087-6.25h3.162a.625.625,0,0,0,.442-1.067ZM8.125,18.753H2.134l.086-.086a1.9,1.9,0,0,0,.118-2.516A4.869,4.869,0,0,1,1.25,13.128c0-3.1,3.084-5.625,6.875-5.625S15,10.026,15,13.128,11.916,18.753,8.125,18.753Zm8.087-6.25c-.375-3.5-3.857-6.25-8.087-6.25a9.362,9.362,0,0,0-3.119.529c.064-3.057,3.118-5.528,6.869-5.528,3.791,0,6.875,2.523,6.875,5.625A4.869,4.869,0,0,1,17.662,9.9a1.9,1.9,0,0,0,.118,2.516l.086.086Z"
                    transform="translate(0)"
                  />
                </g>
              </svg>
              <span className="ml-3 text-sm font-medium"> CHAT </span>
            </NavLink>
            <NavLink
              to="#"
              className="flex items-center px-4 py-2 font-light text-white bg-[#79828B] rounded-md hover:bg-[#42505C]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-current"
              >
                <path
                  className="a"
                  d="M0,4.426H4.426V0H0ZM1.172,1.172H3.254V3.254H1.172Zm0,0"
                />
                <path
                  className="a"
                  d="M149.3,0V4.426h14.168V0Zm13,3.254H150.469V1.172h11.824Zm0,0"
                  transform="translate(-143.465)"
                />
                <path
                  className="a"
                  d="M0,151.434H4.426v-4.426H0Zm1.172-3.254H3.254v2.082H1.172Zm0,0"
                  transform="translate(0 -141.266)"
                />
                <path
                  className="a"
                  d="M149.3,151.434h14.168v-4.426H149.3Zm1.172-3.254h11.824v2.082H150.469Zm0,0"
                  transform="translate(-143.465 -141.266)"
                />
                <path
                  className="a"
                  d="M0,298.438H4.426v-4.426H0Zm1.172-3.254H3.254v2.082H1.172Zm0,0"
                  transform="translate(0 -282.527)"
                />
                <path
                  className="a"
                  d="M149.3,298.438h14.168v-4.426H149.3Zm1.172-3.254h11.824v2.082H150.469Zm0,0"
                  transform="translate(-143.465 -282.527)"
                />
              </svg>
              <span className="ml-3 text-sm font-medium"> CATEGORY </span>
            </NavLink>
            <NavLink
              to="#"
              className="flex items-center px-4 py-2 font-light text-white bg-[#79828B] rounded-md hover:bg-[#42505C]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-current"
              >
                <g transform="translate(0 -34.445)">
                  <path
                    className="a"
                    d="M275.483,172.228l0,4.373a.625.625,0,0,0,.183.442l3.128,3.127.884-.884-2.945-2.944,0-4.114Z"
                    transform="translate(-265.484 -132.783)"
                  />
                  <path
                    className="a"
                    d="M11.25,34.445a8.756,8.756,0,0,0-8.65,7.5H0L3.125,45.07,6.25,41.945H3.863a7.487,7.487,0,1,1,1.473,5.863l-.985.769a8.75,8.75,0,1,0,6.9-14.132Z"
                  />
                </g>
              </svg>
              <span className="ml-3 text-sm font-medium"> HISTORY </span>
            </NavLink>
            <NavLink
              to="#"
              className="flex items-center px-4 py-2 font-light text-white bg-[#79828B] rounded-md hover:bg-[#42505C]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-current"
              >
                <g transform="translate(-34.445)">
                  <path
                    className="a"
                    d="M143.5,11.429a5.714,5.714,0,1,1,5.714-5.714A5.721,5.721,0,0,1,143.5,11.429Zm0-10a4.286,4.286,0,1,0,4.286,4.286A4.29,4.29,0,0,0,143.5,1.429Z"
                    transform="translate(-99.051)"
                  />
                  <path
                    className="a"
                    d="M53.731,321.13H35.159a.714.714,0,0,1-.714-.714V315.5a2.142,2.142,0,0,1,.984-1.811,17.451,17.451,0,0,1,18.031,0,2.143,2.143,0,0,1,.985,1.811v4.917A.714.714,0,0,1,53.731,321.13ZM35.874,319.7H53.016v-4.2a.7.7,0,0,0-.313-.6,15.994,15.994,0,0,0-16.517,0,.7.7,0,0,0-.312.6Z"
                    transform="translate(0 -298.273)"
                  />
                </g>
              </svg>
              <span className="ml-3 text-sm font-medium"> PROFILE </span>
            </NavLink>
          </nav>
        </div>
        <div className="px-2 mb-6">
          <div className="flex flex-col justify-center space-y-2 card-secondary w-50 py-6">
            <p className="text-sm">
              Upgrade to PRO to get access all Features!
            </p>
            <div className="p-1">
              <button className="bg-white text-sm rounded-lg text-primary-500 font-bold px-6 py-1">
                Get Pro Now!
              </button>
            </div>
          </div>
        </div>
      </div>
    </aside>
  );
}
