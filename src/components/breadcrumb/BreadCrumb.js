import React from "react";
import { ChevronRightIcon } from "@heroicons/react/24/solid";
import { Link, useLocation, useNavigate } from "react-router-dom";

const BreadCrumb = ({ dashboard }) => {
  let location = useLocation();
  const navigate = useNavigate();
  const dataRouter = location.pathname.split("/");
  return (
    <div
      className={`flex items-center text-base text-textSecondary px-2 space-x-1 ${
        dashboard === "/" ? "hidden" : "block"
      }`}
    >
      <div>
        <Link to={"/"}>
          <span>Dashboard</span>
        </Link>
      </div>
      <div className="flex items-center">
        <ChevronRightIcon
          className={`w-4 h-4 ${dataRouter.length === 1 ? "hidden" : "block"}`}
        />
        <button
          className="capitalize"
          onClick={() => navigate(`/${dataRouter[1]}`, { replace: true })}
        >
          {dataRouter[1]}
        </button>
      </div>
      <div className="flex items-center">
        <ChevronRightIcon
          className={`w-4 h-4 ${dataRouter.length === 2 ? "hidden" : "block"}`}
        />
        <button
          className="capitalize"
          onClick={() =>
            navigate(`${dataRouter[1]}/${dataRouter[2]}`, { replace: true })
          }
        >
          {dataRouter[2]}
        </button>
      </div>
    </div>
  );
};
export default BreadCrumb;
