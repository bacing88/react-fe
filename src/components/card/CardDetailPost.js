import React from "react";
import { useSelector } from "react-redux";

function CardDetailPost(id) {
  const { value } = useSelector((state) => state.identitas);
  const data = useSelector((state) => state.posts.entities[value]);
  return (
    <>
      <div>Detail Post</div>
      <ul className="sticky top-4 list-disc list-inside p-4 pb-6 bg-gray-100 mt-2 rounded-md space-y-2">
        <li>{data?.id ?? "state redux di hapus "}</li>
        <li>{data?.email ?? "state redux di hapus "}</li>
        <li>{data?.name ?? "state redux di hapus "}</li>
        <li>{data?.body ?? "state redux di hapus "}</li>
      </ul>
    </>
  );
}

export default CardDetailPost;
