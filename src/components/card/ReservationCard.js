import React, { memo } from "react";
import { XCircleIcon } from "@heroicons/react/24/outline";
import { useDispatch } from "react-redux";
import { update, updateHidden } from "../../feature/identitasSlice";

function ReservationCard({ id, name, body, onDelete, onUpdate }) {
  const dispatch = useDispatch();
  return (
    <div className="relative bg-gray-100 rounded-sm px-2 py-1 text-md text-textSecondary mt-2">
      <XCircleIcon
        onClick={() => onDelete(id)}
        className="w-6 h-6 absolute top-2 right-2 stroke-2 text-danger cursor-pointer"
      />
      <p className="mt-4">{name}</p>
      <p className="text-xs mt-0.5">{body}</p>
      <div className="my-2 flex justify-between space-x-4">
        <button
          className="button-primary rounded-md w-1/2"
          onClick={() => {
            dispatch(update(id));
            dispatch(updateHidden("Detail"));
          }}
        >
          Detail
        </button>
        <button
          className="border-2 border-primary-400 rounded-md text-textSecondary text-sm font-semibold w-1/2"
          onClick={() => {
            dispatch(updateHidden("Update"));
            dispatch(
              onUpdate({
                id,
                newObj: {
                  body: "Update State Body",
                  name: "Update State Name",
                },
              })
            );
          }}
        >
          Update
        </button>
      </div>
    </div>
  );
}

export default memo(ReservationCard);
