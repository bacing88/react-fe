export const COLUMNS = [
  {
    Header: "No",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "first_name",
  },
  {
    Header: "Phone Number",
    accessor: "date_of_birth",
  },
  {
    Header: "Status",
    accessor: "country",
  },
];
