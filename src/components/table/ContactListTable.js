import React, { useMemo, useState } from "react";
import { useTable, useSortBy, usePagination } from "react-table";
import MOCK_DATA from "../../data/DiviceTable.json";
import { COLUMNS } from "./ColomnContact";
import {
  ChevronRightIcon,
  ChevronLeftIcon,
  Bars3BottomRightIcon,
  BarsArrowDownIcon,
  BarsArrowUpIcon,
} from "@heroicons/react/24/outline";

export const ContactListTable = () => {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => MOCK_DATA, []);
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(3);
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    page,
    pageOptions,
    gotoPage,
    pageCount,
    prepareRow,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 10 },
    },
    useSortBy,
    usePagination
  );

  const range = [];
  const num = Math.ceil(pageCount);
  for (let i = 1; i <= num; i++) {
    range.push(i);
  }
  const sliceRange = range.slice(start, end);
  return (
    <>
      <div className="overflow-x-auto relative">
        <table
          {...getTableProps()}
          className="w-full table-fixed text-sm text-left text-textSecondary select-none"
        >
          <thead className="text-sm text-cardBg ordinal bg-primary-400">
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    className="py-2 px-3"
                  >
                    <div className="flex justify-between">
                      {column.render("Header")}
                      <span>
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <BarsArrowDownIcon className="w-5 h-5" />
                          ) : (
                            <BarsArrowUpIcon className="w-5 h-5" />
                          )
                        ) : (
                          <Bars3BottomRightIcon className="w-5 h-5" />
                        )}
                      </span>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()} className="">
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()} className="py-1 px-2">
                        {cell.render("Cell")}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
          <tfoot>
            {footerGroups.map((footerGroup) => (
              <tr {...footerGroup.getFooterGroupProps()}>
                {footerGroup.headers.map((column) => (
                  <td {...column.getFooterProps()}>
                    {column.render("Footer")}
                  </td>
                ))}
              </tr>
            ))}
          </tfoot>
        </table>
        <div className="flex justify-end space-x-2">
          <button
            className="p-0.5 text-primary-300 border rounded-sm border-primary-300"
            onClick={() => {
              setStart(start - 3);
              setEnd(end - 3);
            }}
            disabled={start === 0}
          >
            <ChevronLeftIcon className="w-5 h-4 stroke-2" />
          </button>
          {sliceRange.map((page, index) => (
            <button
              className="px-2 py-0.5 text-sm rounded-sm bg-primary-300 text-white"
              key={index}
              onClick={() => gotoPage(page - 1)}
            >
              {page}
            </button>
          ))}
          <button
            className="p-0.5 text-primary-300 border rounded-sm border-primary-300"
            onClick={() => {
              setStart(start + 3);
              setEnd(end + 3);
            }}
            disabled={sliceRange.includes(pageOptions.length)}
          >
            <ChevronRightIcon className="w-5 h-4" />
          </button>
        </div>
      </div>
    </>
  );
};
