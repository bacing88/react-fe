import React, { useState, useEffect } from "react";
import Axios from "axios";
import DeleteUsers from "../../view/contact/deleteUsers";
import moment from "moment";
import ViewUsers from "../../view/contact/viewUsers";
import EditUsers from "../../view/contact/editUsers";

export const UsersListTable = () => {
  const [user, setUser] = useState(null);
  const [load, setLoad] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      setLoad(true);
      await Axios.get(
        "https://firestore.googleapis.com/v1/projects/testing-crud-843a3/databases/(default)/documents/Crud%20Users"
      )
        .then((response) => {
          setUser(response.data);
          setLoad(false);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchData();
  }, []);
  if (!user) return null;
  return (
    <>
      {load ? (
        <p className="flex justify-center items-center text-lg">Loading...</p>
      ) : (
        <div className="overflow-x-auto relative">
          <table className="w-full text-sm text-left text-textSecondary select-none">
            <thead className="text-sm text-cardBg ordinal bg-primary-400">
              <tr>
                <th className="py-2 px-2">No</th>
                <th className="py-2 px-2">Nama</th>
                <th className="py-2 px-2">Alamat</th>
                <th className="py-2 px-2">P / W</th>
                <th className="py-2 px-2">Tanggal Lahir</th>
                <th className="py-2 px-2">Tanggal Input</th>
                <th className="py-2 px-2">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {user?.documents?.map((item, index) => (
                <tr key={index}>
                  <td className="py-1 px-2"> {index + 1}</td>
                  <td className="py-1 px-2">{item.fields.nama.stringValue}</td>
                  <td className="py-1 px-2">
                    {item.fields.alamat.stringValue}
                  </td>
                  <td className="py-1 px-2">
                    {item.fields.gander.stringValue.charAt(0).toUpperCase() +
                      item.fields.gander.stringValue.slice(1)}
                  </td>
                  <td className="py-1 px-2">
                    {moment
                      .unix(item.fields.birth.stringValue)
                      .format("DD MMM. YYYY")}
                  </td>
                  <td className="mx-auto">
                    {moment(item.createTime).format("DD MMM. YYYY hh:mm")}
                  </td>
                  <td className="py-1 px-2">
                    <div className="flex space-x-1">
                      <ViewUsers todo={item} />
                      <EditUsers todo={item} />
                      <DeleteUsers todo={item.name} />
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};
