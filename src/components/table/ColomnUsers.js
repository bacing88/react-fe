import {
  PlusCircleIcon,
  PencilSquareIcon,
  TrashIcon,
  EyeIcon,
} from "@heroicons/react/24/outline";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import UsersList from "../form/usersList";

export default function MyModal() {
  let [isOpen, setIsOpen] = useState(false);
  let [action, setAction] = useState("show");

  function openModal() {
    setIsOpen(true);
    setAction("Show");
  }
  function openModal1() {
    setIsOpen(true);
    setAction("Delete");
  }
  function openModal2() {
    setIsOpen(true);
    setAction("Edit");
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <>
      <div className="flex space-x-1">
        <div>
          <button
            type="button"
            onClick={openModal}
            className="bg-green-300 p-0.5 rounded-md text-green-600"
          >
            <EyeIcon className="h-4 w-5" />
          </button>
        </div>
        <div>
          <button
            type="button"
            onClick={openModal1}
            className="bg-red-300 p-0.5 rounded-md text-red-600"
          >
            <TrashIcon className="h-4 w-5" />
          </button>
        </div>
        <div>
          <button
            type="button"
            onClick={openModal2}
            className="bg-blue-300 p-0.5 rounded-md text-blue-600"
          >
            <PencilSquareIcon className="h-4 w-5" />
          </button>
        </div>
      </div>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title className="text-lg font-medium leading-6 text-gray-900">
                    {action === "Delete" ? "Delete" : "Users"}
                  </Dialog.Title>
                  <div className="mt-4">
                    <div className="card-primary">
                      {/* Form */}
                      <UsersList />
                    </div>
                  </div>

                  <div className="mt-4">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                      onClick={closeModal}
                    >
                      {action}
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
export const COLUMNS = [
  {
    Header: "No",
    accessor: "createTime",
  },
  {
    Header: "Name",
    accessor: "updateTime",
  },
  {
    Header: "Aksi",
    Cell: () => MyModal(),
  },
];
