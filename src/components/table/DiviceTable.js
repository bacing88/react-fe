import React, { useMemo, useState } from "react";
import { useTable, usePagination } from "react-table";
import MOCK_DATA from "../../data/DiviceTable.json";
import { COLUMNS } from "./ColomnDivice";
import { ChevronRightIcon, ChevronLeftIcon } from "@heroicons/react/24/outline";

export const DiviceTable = () => {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => MOCK_DATA, []);
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(3);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    pageOptions,
    gotoPage,
    pageCount,
    prepareRow,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 3 },
    },
    usePagination
  );
  const range = [];
  const num = Math.ceil(pageCount);
  for (let i = 1; i <= num; i++) {
    range.push(i);
  }
  const sliceRange = range.slice(start, end);
  return (
    <>
      <table {...getTableProps()} className="table-auto w-full">
        <thead className="text-sm text-textSecondary">
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  {...column.getHeaderProps()}
                  className="text-md text-textSecondary font-medium"
                >
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()} className="text-xs text-textSecondary">
          {page.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()} className="text-center">
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()} className="p-2">
                      {cell.render("Cell")}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="flex justify-end space-x-2">
        <button
          className="p-0.5 text-primary-300 border rounded-sm border-primary-300"
          onClick={() => {
            setStart(start - 3);
            setEnd(end - 3);
          }}
          disabled={start === 0}
        >
          <ChevronLeftIcon className="w-5 h-4 stroke-2" />
        </button>
        {sliceRange.map((page, index) => (
          <button
            className="px-2 py-0.5 text-sm rounded-sm bg-primary-300 text-white"
            key={index}
            onClick={() => gotoPage(page - 1)}
          >
            {page}
          </button>
        ))}
        <button
          className="p-0.5 text-primary-300 border rounded-sm border-primary-300"
          onClick={() => {
            setStart(start + 3);
            setEnd(end + 3);
          }}
          disabled={sliceRange.includes(pageOptions.length)}
        >
          <ChevronRightIcon className="w-5 h-4" />
        </button>
      </div>
    </>
  );
};
