/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";

function cardReal() {
  const [current, setCurrent] = React.useState(1);
  React.useEffect(() => {
    if (current === 5) setCurrent(1);
    document
      .querySelector(`#slide-${current > 4 ? 1 : current}`)
      .scrollIntoView();
  }, [current]);

  return (
    <div className="mx-2 lg:mx-64 ">
      <img
        alt="Lava"
        src="https://images.unsplash.com/photo-1631451095765-2c91616fc9e6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80"
        className="h-56 w-full rounded-xl object-cover shadow-xl"
      />
      <div className="snap-x mx-auto snap-mandatory flex w-96 overflow-x-hidden space-x-2 m-4 mt-8 ">
        <div
          id="slide-1"
          className="snap-center snap-always w-80 flex-shrink-0  flex items-center justify-center text-xld"
        >
          <img
            src="https://images.unsplash.com/photo-1604999565976-8913ad2ddb7c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=320&h=160&q=80"
            alt="imag"
            className=" rounded-lg"
          />
        </div>
        <div
          id="slide-2"
          className="scroll-mx-6 snap-center snap-always w-80 flex-shrink-0   flex items-center justify-center text-xl"
        >
          <img
            src="https://images.unsplash.com/photo-1622890806166-111d7f6c7c97?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=320&h=160&q=80"
            alt="imag"
            className=" rounded-lg"
          />
        </div>
        <div
          id="slide-3"
          className="scroll-mx-6 snap-center snap-always w-80 flex-shrink-0  flex items-center justify-center text-xl"
        >
          <img
            src="https://images.unsplash.com/photo-1540206351-d6465b3ac5c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=320&h=160&q=80"
            alt="imag"
            className=" rounded-lg"
          />
        </div>
        <div
          id="slide-4"
          className="scroll-mx-6 snap-center snap-always w-80 flex-shrink-0  flex items-center justify-center text-xl"
        >
          <img
            src="https://images.unsplash.com/photo-1575424909138-46b05e5919ec?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=320&h=160&q=80"
            alt="imag"
            className=" rounded-lg"
          />
        </div>
      </div>

      {/* <div>
          <button
            onClick={() => {
              setCurrent((cur) => cur + 1);
            }}
            className="w-32 bg-red-300 rounded-sm"
          >
            Next
          </button>
        </div> */}
      <div className="flex justify-center items-center space-x-2">
        {[1, 2, 3, 4].map((item, index) => (
          <button
            onClick={() => setCurrent(index + 1)}
            className="bg-gray-300 h-2 w-2 rounded-full"
          ></button>
        ))}
      </div>

      {/* <div>
          <button
            onClick={() => {
              setCurrent((cur) => cur - 1);
            }}
            className="w-32 bg-red-300 rounded-sm"
            disabled={current === 1}
          >
            Prev
          </button>
        </div> */}

      <div className="p-2">
        <p className="text-[#79828B] text-sm font-thin">
          1 Rooms . 1 BathRooms . 250M2
        </p>
        <h5 className="text-xl font-bold text-[#42505C] mt-2">
          Rumah Tipe 68 Jalan Petukangan Jakarta Selatan , Budi Hasian
        </h5>
        <div className="flex space-x-2 mt-3">
          <div className="bg-[#E4E4E4] rounded-lg text-[#79828B] font-thin text-sm text-center py-1 px-5">
            Detai
          </div>
          <div className="bg-white rounded-lg text-[#79828B] font-thin text-sm text-center py-1 px-5">
            Map
          </div>
          <div className="bg-white rounded-lg text-[#79828B] font-thin text-sm text-center py-1 px-5">
            Load
          </div>
        </div>
      </div>
      <div className="flex flex-col divide-y-2 p-2">
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Type of Property</p>
          <p className="text-md font-bold text-[#42505C] mb-3">House</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Status</p>
          <p className="text-md font-bold text-[#42505C] mb-3">Rent</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Certification</p>
          <p className="text-md font-bold text-[#42505C] mb-3">....</p>
        </div>
      </div>
      <div className="mx-4 px-3 bg-[#42505C] text-white py-2 rounded-sm w-64">
        Contact Landlord
      </div>
      <div className="flex flex-col divide-y-2 p-2">
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Area</p>
          <p className="text-md font-bold text-[#42505C] mb-3">250M2</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Building Size</p>
          <p className="text-md font-bold text-[#42505C] mb-3">300M2</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Full Furnished</p>
          <p className="text-md font-bold text-[#42505C] mb-3">Yes</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Garage</p>
          <p className="text-md font-bold text-[#42505C] mb-3">Yes</p>
        </div>
        <div className="px-3">
          <p className="text-[#79828B] text-sm font-thin">Carport</p>
          <p className="text-md font-bold text-[#42505C] mb-3">Not Available</p>
        </div>
      </div>
    </div>
  );
}

export default cardReal;
