import React from "react";
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

export default function UsersList() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);
  console.log(errors);

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label className="text-gray-600">
          Nama : <span className="text-red-500">*</span>
        </label>
        <input
          className="input-primary"
          type="text"
          {...register("nama", { required: "This is required." })}
        />
        <ErrorMessage
          errors={errors}
          name="nama"
          render={({ message }) => (
            <p className="mb-1 text-normal text-red-500">{message}</p>
          )}
        />
        <label className="text-gray-600">
          Alamat : <span className="text-red-500">*</span>
        </label>
        <input
          className="input-primary"
          type="text"
          {...register("alamat", { required: "This is required." })}
        />
        <ErrorMessage
          errors={errors}
          name="alamat"
          render={({ message }) => (
            <p className="mb-1 text-normal text-red-500">{message}</p>
          )}
        />
        <div className="flex mt-4 mb-3">
          <div className="pr-4">
            <label className="text-gray-600">P / W : </label>
          </div>
          <div className="space-x-2">
            <input
              {...register("P / W", { required: true })}
              type="radio"
              value="Pria"
            />
            <label className="peer-checked/draft:text-sky-500">Pria</label>

            <input
              className="accent-pink-500"
              {...register("P / W", { required: true })}
              type="radio"
              value="Wanita"
            />
            <label className="peer-checked/draft:text-sky-500">Wanita</label>
          </div>
        </div>
        <label className="text-gray-600">
          Tanggal Lahir : <span className="text-red-500">*</span>
        </label>
        <input
          className="input-primary"
          type="datetime-local"
          placeholder="Tanggal Lahir"
          {...register("Tanggal Lahir", {})}
        />

        <input
          className="mt-4 w-full bg-green-400 hover:bg-green-600 text-green-100 border py-3 px-6 font-semibold text-md rounded"
          type="submit"
        />
      </form>
    </>
  );
}
