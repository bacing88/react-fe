import React from "react";
import {
  Chart as ChartJS,
  RadialLinearScale,
  ArcElement,
  Tooltip,
  Legend,
} from "chart.js";
import { PolarArea } from "react-chartjs-2";

ChartJS.register(RadialLinearScale, ArcElement, Tooltip, Legend);
export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "right",
      labels: {
        boxWidth: 8,
        boxHeight: 8,
        usePointStyle: true,
      },
    },
  },
};
export const data = {
  labels: [
    "No response",
    "Budget Issue",
    "Choose Competitor",
    "Not interested",
    "Other",
  ],
  datasets: [
    {
      label: "Lost Reason",
      data: [4.3, 3.5, 4, 6.1, 3.6],
      backgroundColor: [
        "rgba(46, 230, 202, 1)",
        "rgba(41, 207, 155, 1)",
        "rgba(91, 244, 110, 1)",
        "rgb(26, 119, 111, 1)",
        "rgba(45, 137, 87, 1)",
      ],
      borderWidth: 1,
    },
  ],
};

const LostReason = () => {
  return (
    <div className="flex justify-center p-2">
      <div className="w-3/4">
        <PolarArea options={options} data={data} />
      </div>
    </div>
  );
};
export default LostReason;
