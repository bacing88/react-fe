import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { faker } from "@faker-js/faker";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,

  plugins: {
    scales: {
      xAxes: [
        {
          gridLines: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            display: false,
          },
        },
      ],
    },
    legend: {
      position: "top",
      labels: {
        boxWidth: 8,
        boxHeight: 8,
        usePointStyle: true,
      },
    },
  },
};

const labels = ["01/08", "02/08", "03/08", "04/08", "05/08", "06/08", "07/08"];

export const data = {
  labels,
  datasets: [
    {
      label: "Data Message",
      data: labels.map(() => faker.datatype.number({ min: 0, max: 10000 })),
      backgroundColor: "rgba(41, 207, 155, 1)",
      borderRadius: 15,
    },
  ],
};

const MessageSentByTeam = () => {
  return (
    <div className="flex items-center h-full">
      <div className="w-full">
        <Bar options={options} data={data} />
      </div>
    </div>
  );
};
export default MessageSentByTeam;
