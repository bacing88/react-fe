import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { faker } from "@faker-js/faker";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
      labels: {
        boxWidth: 8,
        boxHeight: 8,
        usePointStyle: true,
      },
    },
  },
};

const labels = ["23 Mar", "24 Mar", "25 Mar", "26 Mar", "27 Mar"];

export const data = {
  labels,
  datasets: [
    {
      label: "Data Income",
      data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
      borderColor: "rgba(13, 74, 253, 1)",
      backgroundColor: "rgba(13, 74, 253, 1)",
    },
  ],
};

const IncomeStatistics = () => {
  return <Line options={options} data={data} />;
};
export default IncomeStatistics;
