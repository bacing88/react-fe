import React from "react";

function base() {
  return (
    <div className="p-12">
      <h1 className="text-3xl font-bold text-primary-200">Hello world!</h1>
      <h1 className="text-lg text-primary-100">Hello world!</h1>
      <h1 className="text-3xl font-bold text-secondary-200">Hello world!</h1>
      <h1 className="text-lg text-secondary-100">Hello world!</h1>
      <h1 className="text-lg text-danger">Hello world!</h1>
      <h1 className="text-2xl font-bold text-textPrimary">Text Primary</h1>
      <h1 className="text-lg text-textSecondary">Text Secondary</h1>
      <button className="w-16 mb-1 rounded-sm bg-gradient-to-br from-multiColorBtn-200 to-multiColorBtn-100 px-1 text-white">
        button
      </button>
      <div className="card-secondary w-56">
        Upgrade to PRO to get access all Features!
      </div>
      <div className="card-primary">Card</div>
      <div className="card-primary">Card2</div>
    </div>
  );
}

export default base;
