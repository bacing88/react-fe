import React from "react";
import Logo from "../image/logo.jpg";
import {
  HomeIcon,
  ChatBubbleLeftIcon,
  IdentificationIcon,
  MegaphoneIcon,
  BuildingStorefrontIcon,
  ClipboardDocumentCheckIcon,
  DocumentTextIcon,
  UserIcon,
  FilmIcon,
} from "@heroicons/react/24/solid";
import { NavLink, useLocation } from "react-router-dom";

export default function Sidebar() {
  const location = useLocation();
  const dataPath = location.pathname.split("/");
  console.log(dataPath);
  return (
    <aside className="w-52 fixed left-0 top-0  bg-[#FCFCFC] drop-shadow-sm select-none">
      <div className="flex flex-col h-screen justify-between">
        <div className="px-2 py-5">
          <div className="px-10 -mb-2">
            <img src={Logo} width="100" height="40" alt="logo" />
          </div>

          <nav className="flex flex-col mt-6 space-y-1">
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <HomeIcon className="h-5 w-5" />
              <span className="ml-3 text-sm font-medium"> Home </span>
            </NavLink>
            <NavLink
              to="/movies/list-gender"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <FilmIcon className="h-5 w-5" />
              <span className="ml-3 text-sm font-medium"> Movies </span>
            </NavLink>
            <NavLink
              to={
                dataPath[1] === "contact"
                  ? `/contact/${dataPath[2]}`
                  : "/contact/usersList"
              }
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <IdentificationIcon className="h-5 w-5" />
              <span className="ml-3 text-sm font-medium"> Data Users </span>
            </NavLink>
            <NavLink
              to="/users"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <BuildingStorefrontIcon className="h-5 w-5" />

              <span className="ml-3 text-sm font-medium"> Whatsapp Shop </span>
            </NavLink>
            <NavLink
              to="/marketing"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <MegaphoneIcon className="h-5 w-5" />

              <span className="ml-3 text-sm font-medium"> Marketing </span>
            </NavLink>
            <NavLink
              to="/teamInbox"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <ChatBubbleLeftIcon className="h-5 w-5" />

              <span className="ml-3 text-sm font-medium"> Team Inbox </span>
            </NavLink>
            <NavLink
              to="/realEstateFinder"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <ClipboardDocumentCheckIcon className="h-5 w-5" />

              <span className="ml-3 text-sm font-medium">
                Real Estate Finder
              </span>
            </NavLink>
            <NavLink
              to="/report"
              className={({ isActive }) =>
                isActive
                  ? "flex items-center px-4 py-2 button-primary"
                  : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
              }
            >
              <DocumentTextIcon className="h-5 w-5" />

              <span className="ml-3 text-sm font-medium"> Report </span>
            </NavLink>
          </nav>
        </div>
        <div className="px-2">
          <div className="flex flex-col justify-center space-y-2 card-secondary w-50 py-6">
            <p className="text-sm">
              Upgrade to PRO to get access all Features!
            </p>
            <div className="p-1">
              <button className="bg-white text-sm rounded-lg text-primary-500 font-bold px-6 py-1">
                Get Pro Now!
              </button>
            </div>
          </div>
        </div>

        <div className="bottom-2 w-full">
          <button
            href=""
            className="flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          >
            <UserIcon className="h-5 w-5" />

            <span className="ml-3 text-sm font-medium"> Account </span>
          </button>
        </div>
      </div>
    </aside>
  );
}
