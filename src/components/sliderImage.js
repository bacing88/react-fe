import React from "react";
import "tw-elements";
import { MapPinIcon, HomeIcon, CheckIcon } from "@heroicons/react/24/solid";
import { NavLink } from "react-router-dom";

function sliderImage() {
  return (
    <>
      <div
        id="carouselExampleCrossfade"
        className="carousel slide carousel-fade relative font-pop-pins px-2 lg:px-16"
        data-bs-ride="carousel"
      >
        <div className="carousel-indicators absolute right-0 bottom-12 left-0 flex justify-center p-0 -mb-2 sm:mb-16">
          <button
            type="button"
            data-bs-target="#carouselExampleCrossfade"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCrossfade"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCrossfade"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCrossfade"
            data-bs-slide-to="3"
            aria-label="Slide 4"
          ></button>
        </div>
        <div className="carousel-inner relative w-full overflow-hidden rounded-md">
          <div className="carousel-item active float-left w-full">
            <img
              src="https://mdbcdn.b-cdn.net/img/new/slides/041.webp"
              className="block w-full"
              alt="Wild Landscape"
            />
          </div>
          <div className="carousel-item float-left w-full">
            <img
              src="https://mdbcdn.b-cdn.net/img/new/slides/042.webp"
              className="block w-full"
              alt="Camera"
            />
          </div>

          <div className="carousel-item float-left w-full">
            <img
              src="https://mdbcdn.b-cdn.net/img/new/slides/043.webp"
              className="block w-full"
              alt="Exotic Fruits"
            />
          </div>
          <div className="carousel-item float-left w-full">
            <img
              src="https://mdbcdn.b-cdn.net/img/new/slides/046.webp"
              className="block w-full"
              alt="Exotic Fruits"
            />
          </div>
        </div>
        <div className="py-0 md:py-6 flex lg:flex-row flex-col space-y-3 lg:space-x-4 lg:space-y-0">
          <button className="bg-gray-100 inline-flex py-3 px-5 rounded-lg items-center ">
            <MapPinIcon className="h-10 w-8 text-[#42505C]" />
            <span className="ml-4 flex items-start flex-col leading-none">
              <span className="text-xs text-[#79828B] mb-1">Pick Location</span>
              <span className="text-md font-bold text-[#42505C]">
                Jakarta Selatan
              </span>
            </span>
          </button>
          <button className="bg-gray-100 inline-flex py-3 px-5 rounded-lg items-center">
            <HomeIcon className="h-10 w-8 text-[#42505C]" />

            <span className="ml-4 flex items-start flex-col leading-none">
              <span className="text-xs text-[#79828B] mb-1">
                Type of Property
              </span>
              <span className="text-md font-bold text-[#42505C]">House</span>
              <div className="divide-y divide-amber-800"></div>
            </span>
          </button>
          <button className="bg-gray-100 inline-flex sm:w-64 py-3 px-5 rounded-lg items-center ">
            <CheckIcon className="h-10 w-8 text-[#42505C]" />
            <span className="ml-4 flex items-start flex-col leading-none">
              <span className="text-xs text-[#79828B] mb-1">Status</span>
              <span className="text-md font-bold text-[#42505C] border-b-2">
                Rent
              </span>
            </span>
          </button>
          <NavLink
            to="/realEstateFinder/12"
            className="bg-[#42505C] text-white rounded-md p-5 text-center hover:bg-[#79828B] focus:outline-none"
          >
            Detail
          </NavLink>
        </div>
      </div>
    </>
  );
}

export default sliderImage;
