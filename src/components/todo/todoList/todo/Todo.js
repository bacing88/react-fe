import React from "react";
import { useDispatch } from "react-redux";
import { updateTodo, deleteTodo } from "../../../../feature/todoSlice";
import { XCircleIcon } from "@heroicons/react/24/outline";

const Todo = ({ text, completed, id }) => {
  const dispatch = useDispatch();
  const toggleItem = () => {
    dispatch(
      updateTodo({ id, changes: { completed: !completed, todo: "testing" } })
    );
  };
  const deleteItem = () => {
    dispatch(deleteTodo(id));
  };
  return (
    <div className="flex justify-between">
      <div className="flex items-center space-x-2">
        <input
          className="accent-primary-400"
          type="checkbox"
          value={completed}
          onChange={toggleItem}
        />
        <span>{text}</span>
      </div>
      <XCircleIcon className="w-5 h-5 text-danger" onClick={deleteItem} />
    </div>
  );
};
export default Todo;
