import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  restoreTodo,
  clearTodos,
  todoSelectors,
} from "../../../feature/todoSlice";
import Todo from "./todo/Todo";
function TodoList() {
  const dispatch = useDispatch();
  const allTodos = useSelector(todoSelectors.selectEntities);
  const todoCount = useSelector(todoSelectors.selectTotal);
  const deletedTodos = useSelector((state) => state.todos.deletedTodos);

  const todoList = [];
  for (const id in allTodos) {
    if (Object.hasOwnProperty.call(allTodos, id)) {
      const todoItem = allTodos[id];
      todoList.push(
        <Todo
          key={todoItem.id}
          id={todoItem.id}
          text={todoItem.todo}
          completed={todoItem.completed}
        />
      );
    }
  }
  const restore = (todo) => {
    dispatch(restoreTodo(todo));
  };
  const deletedList = deletedTodos.map((todo) => (
    <div className="flex justify-between mb-1" key={todo.id}>
      <span>{todo.todo}</span>
      <button
        className="text-sm text-secondary-200 border border-secondary-200 rounded-md px-2 py-0.5"
        onClick={() => restore(todo)}
      >
        Restore
      </button>
    </div>
  ));
  const clearList = () => {
    dispatch(clearTodos());
  };
  return (
    <div className="flex flex-col space-y-1 items-center py-3">
      <h3 className="text-md font-semibold">Your Teams</h3>
      <h4>Count Team Active: {todoCount}</h4>
      <button
        className="px-2 py-1 text-white bg-danger text-sm"
        onClick={clearList}
      >
        Clear All Teams
      </button>
      <div className="w-96">
        <h3>{todoList}</h3>
      </div>
      {deletedList.length === 0 ? null : (
        <div className="w-96 bg-gray-200 px-5 py-2 rounded-sm">
          <h3 className="text-md font-semibold mb-2">Team NoActive</h3>
          <div>{deletedList}</div>
        </div>
      )}
    </div>
  );
}

export default TodoList;
