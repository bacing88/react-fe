import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodos } from "../../../feature/todoSlice";
import { nanoid } from "@reduxjs/toolkit";

const AddTodo = () => {
  const dispatch = useDispatch();
  const [text, setText] = useState("");

  const submit = () => {
    const items = text.split(",");
    dispatch(
      addTodos(
        items.map((item) => ({ id: nanoid(), todo: item, completed: false }))
      )
    );
  };
  return (
    <div className="flex flex-col items-center">
      <p className="text-textPrimary text-md font-semibold mt-4">
        To add multiple Teams write them comma separated
      </p>
      <p className="text-textSecondary text-sm font-extralight">
        eg: Bacing, Jhon, Sara, Budi
      </p>
      <div className="py-2 w-64">
        <input
          className="appearance-none border rounded w-full py-2 px-3 text-textSecondary leading-tight focus:outline-none focus:shadow-outline"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <button
          className="py-1.5 button-primary rounded-sm w-full mt-2"
          onClick={() => submit()}
        >
          Add Team
        </button>
      </div>
    </div>
  );
};

export default AddTodo;
