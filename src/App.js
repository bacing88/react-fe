import React from "react";
import Home from "./view/home";

import WhatsappShop from "./view/whatsappShop";
import Marketing from "./view/marketing";
import TeamInbox from "./view/teamInbox";
import Report from "./view/report";
import NoMatch from "./noMacht";

//movies
import Movies from "./view/movies";
import ListGender from "./view/movies/ListGender";
import ListMovie from "./view/movies/ListMovie";
import DetailMovie from "./view/movies/DetailMovies";

//contacts
import Contact from "./view/contact";
import UsersList from "./view/contact/usersList";
import ContactList from "./view/contact/contactList";
import ContactScrapping from "./view/contact/contactScrapping";
// users
import Users from "./view/users";
import Detail from "./view/users/detail";
import AddPosts from "./view/users/AddPosts";

//Real Estate Finder
import RealEstateFinder from "./view/mobile/Dashboard";
import DetailReal from "./view/mobile/detail";

import { Routes, Route, useLocation } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import SidebarReal from "./components/SidebarReal";
import NavBar from "./components/NavBar";
//import Breadcrumb from "./components/breadcrumb/BreadCrumb";
export default function App() {
  const location = useLocation();
  const dataPath = location.pathname.split("/");
  console.log(dataPath);
  return (
    <>
      <div className="font-lexend-deca">
        <div className="hidden md:block">
          {location.pathname === "/realEstateFinder" ||
          location.pathname === `/realEstateFinder/${dataPath[2]}` ? (
            <SidebarReal />
          ) : (
            <Sidebar />
          )}
        </div>
        <main className="flex-1 md:ml-52 sm:p-3 space-y-3">
          {location.pathname === "/realEstateFinder" ||
          location.pathname === `/realEstateFinder/${dataPath[2]}` ? null : (
            <NavBar />
          )}

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="contact" element={<Contact />}>
              <Route path="usersList" element={<UsersList />} />
              <Route path="contactList" element={<ContactList />} />
              <Route path="contactScrapping" element={<ContactScrapping />} />
            </Route>
            <Route path="movies/:id" element={<DetailMovie />} />
            <Route path="movies" element={<Movies />}>
              <Route path="/movies/list-gender" element={<ListGender />} />
              <Route path="/movies/list-movie" element={<ListMovie />} />
            </Route>
            <Route path="users" element={<Users />} />
            <Route path="users/:id" element={<Detail />} />
            <Route path="users/add" element={<AddPosts />} />
            <Route path="whatsappShop" element={<WhatsappShop />} />
            <Route path="marketing" element={<Marketing />} />

            <Route path="teamInbox" element={<TeamInbox />} />
            <Route path="realEstateFinder" element={<RealEstateFinder />} />
            <Route path="realEstateFinder/:id" element={<DetailReal />} />
            <Route path="report" element={<Report />} />
            <Route path="*" element={<NoMatch />} />
          </Routes>
        </main>
      </div>
    </>
  );
}
