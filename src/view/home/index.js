import React from "react";
import {
  ChevronDownIcon,
  ChevronRightIcon,
  InformationCircleIcon,
  DocumentDuplicateIcon,
  PlusCircleIcon,
} from "@heroicons/react/24/outline";
import Whatsapp from "../../image/whatsapp-logo.png";
import Facebook from "../../image/facebook_logo.png";
import Twitter from "../../image/Twitter-Logo-Square.png";
import IncomeStatistics from "../../components/chart/IncomeStatistics";
import MessageSentByTeam from "../../components/chart/MessageSentByTeam";
import DealByStageByTime from "../../components/chart/DealByStageByTime";
import LostReason from "../../components/chart/LostReason";

import { DiviceTable } from "../../components/table/DiviceTable";
import { TopProductTable } from "../../components/table/TopProductTable";
const Home = () => {
  return (
    <>
      <div className="flex justify-between items-center px-2 -mt-4">
        <p className="text-base text-textSecondary">Dashboard</p>
        <button className="flex border rounded-md border-primary-500 px-3 py-1.5">
          <span className="mr-2 text-sm text-primary-500 font-medium">
            Last 7 Day
          </span>
          <ChevronDownIcon className="h-5 w-5" />
        </button>
      </div>
      <div className="grid grid-cols-3 gap-3">
        <div className="card-primary p-4 w-full space-y-1">
          <div className="flex justify-between items-center mb-6">
            <div>
              <p className="text-lg text-textPrimary font-semibold">Income</p>
            </div>
            <div>
              <p className="flex items-center space-x-1 text-textSecondary">
                <InformationCircleIcon className="w-5 h-5" />
                <span className="text-xs font-light italic">
                  Nothing Include Postage
                </span>
              </p>
            </div>
          </div>
          <div className="text-3xl text-primary-300 font-bold">RM 18.00</div>
          <button className="flex items-center">
            <span className="mr-2 text-xs text-primary-300 font-thin">
              View Statistics
            </span>
            <ChevronRightIcon className="h-4 w-4 mt-0.5 stroke-2 text-primary-300 bg-[#F0F0F0] rounded-full p-0.5" />
          </button>
        </div>

        <div className="card-primary p-4 w-full space-y-1">
          <div className="flex justify-between items-center mb-6">
            <div>
              <p className="text-lg text-textPrimary font-semibold">
                Shop View
              </p>
            </div>
            <div>
              <p className="flex items-center space-x-1 text-textSecondary">
                <InformationCircleIcon className="w-5 h-5" />
                <span className="text-xs font-light italic">
                  Nothing Include Postage
                </span>
              </p>
            </div>
          </div>
          <div className="text-3xl text-primary-300 font-bold">3X</div>
          <button className="flex items-center">
            <span className="mr-2 text-xs text-primary-300 font-thin">
              View Statistics
            </span>
            <ChevronRightIcon className="h-4 w-4 mt-0.5 stroke-2 text-primary-300 bg-[#F0F0F0] rounded-full p-0.5" />
          </button>
        </div>
        <div className="card-primary p-4 w-full space-y-1">
          <div className="flex justify-between items-center mb-6">
            <div>
              <p className="text-lg text-textPrimary font-semibold">
                Total Transaction
              </p>
            </div>
            <div>
              <p className="flex items-center space-x-1 text-textSecondary">
                <InformationCircleIcon className="w-5 h-5" />
                <span className="text-xs font-light italic">
                  Nothing Include Postage
                </span>
              </p>
            </div>
          </div>
          <div className="text-3xl text-primary-300 font-bold">34</div>
          <button className="flex items-center">
            <span className="mr-2 text-xs text-primary-300 font-thin">
              View Statistics
            </span>
            <ChevronRightIcon className="h-4 w-4 mt-0.5 stroke-2 text-primary-300 bg-[#F0F0F0] rounded-full p-0.5" />
          </button>
        </div>
      </div>
      <div className="card-primary px-6 py-4 w-3/5 space-y-2">
        <p className="text-lg text-textPrimary font-semibold">Get More Order</p>
        <p className="text-sm text-textSecondary">
          Share your store link with shoppers, they can buy through that link.
        </p>
        <div className="relative block">
          <input
            value="realmchat.id/anita_shop"
            className="px-4 py-2 text-secondary-200 rounded-md bg-[#F6F7FF] w-full outline-none"
          />
          <DocumentDuplicateIcon className="w-5 h-5 text-secondary-200 absolute top-2 right-4" />
        </div>
        <p className="text-primary-500 font-medium">Or</p>
        <div className="flex space-x-2 mb-2">
          <img
            src={Whatsapp}
            alt="facebook"
            className="rounded-full w-10 h-10 p-1"
          />
          <img
            src={Facebook}
            alt="facebook"
            className="rounded-full w-10 h-10 p-1"
          />
          <img
            src={Twitter}
            alt="facebook"
            className="rounded-full w-10 h-10 p-1"
          />
        </div>
      </div>

      <div className="card-primary px-6 py-4 w-full space-y-4">
        <div className="flex justify-between">
          <p className="text-lg text-textPrimary font-semibold">Divice List</p>
          <button className="flex items-center space-x-2 button-primary">
            <PlusCircleIcon className="h-5 w-5" />
            <span className="mr-3 text-sm font-medium"> Add Divice </span>
          </button>
        </div>
        <DiviceTable />
      </div>
      <div className="flex space-x-2">
        <div className="card-primary p-4 w-1/3">
          <p className="text-lg text-textPrimary font-semibold">
            Income Statistics
          </p>
          <IncomeStatistics />
        </div>
        <div className="card-primary p-4 w-2/3 space-y-4">
          <p className="text-lg text-textPrimary font-semibold">Top Product</p>
          <TopProductTable />
        </div>
      </div>
      <div className="flex space-x-2">
        <div className="card-primary p-4 w-2/5">
          <p className="text-lg text-textPrimary font-semibold">
            Message sent by team
          </p>
          <MessageSentByTeam />
        </div>
        <div className="card-primary px-6 py-4 w-3/5">
          <p className="text-lg text-textPrimary font-semibold">Lost Reason</p>
          <div className="-my-14">
            <LostReason />
          </div>
        </div>
      </div>
      <div className="card-primary p-4 w-full">
        <p className="text-lg text-textPrimary font-semibold">
          Deal by Stage by Time
        </p>
        <DealByStageByTime />
      </div>
    </>
  );
};
export default Home;
