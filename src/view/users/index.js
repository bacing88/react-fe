import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getUsers, usersSelectors } from "../../feature/usersSlice";
import AllUsers from "./AllUsers";
function Users() {
  const dispatch = useDispatch();
  const users = useSelector(usersSelectors.selectAll);
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const handleClick = async (id) => {
    navigate(`/users/${id}`);
  };

  return (
    <>
      <button
        onClick={() => navigate("/users/add")}
        className="card-secondary text-sm font-semibold py-1 px-3"
      >
        Add Post
      </button>
      <div className="space-y-2">
        <p className="text-md card-secondary rounded-none p-2">
          Get Post Redux
        </p>
        {users.slice(0, 10).map((item) => {
          return (
            <div
              key={item.id}
              className="flex card-primary justify-between items-center text-md px-4"
            >
              <div className="flex flex-col">
                <p className="text-md font-semibold mb-1">{item.title}</p>
                <p className="text-sm font-light">{item.body}</p>
              </div>
              <span className="ml-2">
                <button
                  onClick={() => handleClick(item.id)}
                  className="button-primary py-0.5 px-4"
                >
                  Detail
                </button>
              </span>
            </div>
          );
        })}
      </div>
      <AllUsers />
    </>
  );
}

export default Users;
