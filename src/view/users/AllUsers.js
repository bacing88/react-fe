import React from "react";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const AllUsers = () => {
  const navigate = useNavigate();
  //const queryClient = useQueryClient();
  const { isLoading, error, data, isFetching, fetchStatus } = useQuery(
    ["usersData"],
    () =>
      axios
        .get("https://314e-103-154-141-10.ap.ngrok.io/api/provinsi")
        .then((res) => res.data)
  );

  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;
  //const dataCache = queryClient.getQueriesData(["usersData"]);

  return (
    <div>
      <button className="card-secondary text-sm font-semibold py-1 px-3">
        {fetchStatus}
      </button>
      <p className="text-md card-secondary rounded-none p-2 mb-2">
        Get Post React Query
      </p>
      <div className="space-y-2">
        {data?.map((user, index) => (
          <div key={index}>
            <div className="flex justify-between items-center p-4 card-primary">
              <p className="text-sm">👀 {user.kelurahan}</p>
              <button
                onClick={() => navigate(`/users/${user.id}`)}
                className="button-primary py-0.5 px-4"
              >
                Detail
              </button>
            </div>
          </div>
        ))}
      </div>

      <div>{isFetching ? "Updating..." : ""}</div>
    </div>
  );
};

export default AllUsers;
