import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

function Detail() {
  const { id } = useParams();

  //Redux
  const dataUpdate = useSelector((state) => state.users.entities[id]);

  //React Query

  const { isLoading, error, data } = useQuery(["usersData", id], () =>
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => res.data)
  );
  if (isLoading) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  return (
    <>
      <div className="grid grid-cols-2 gap-4">
        <div className="px-6 py-4 card-primary text-base space-y-4">
          <div>Detail Redux</div>
          <div className="flex space-x-2 items-center">
            <p>Redux DatabyId = {dataUpdate?.title}</p>
          </div>
        </div>
        <div className="px-6 py-4 card-primary text-base space-y-4">
          <div>Detail React Query </div>
          <div className="flex space-x-2 items-center">
            <p>{data?.title}</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Detail;
