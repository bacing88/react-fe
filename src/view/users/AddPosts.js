import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { saveUsers } from "../../feature/usersSlice";

const AddPosts = () => {
  const [title, setTitle] = useState();
  const [body, setBody] = useState();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handelAdd = async (e) => {
    e.preventDefault();
    await dispatch(saveUsers({ title, body }));
    navigate(-1);
  };
  return (
    <div className="container mx-auto w-1/2 px-6 py-4">
      <div className="card-primary p-6">
        <p className="text-lg text-textPrimary font-semibold">
          Contact Scrapping
        </p>
        <form onSubmit={handelAdd}>
          <div className="m-6">
            <label className="block mb-1 text-sm font-medium text-textSecondary">
              Title
            </label>
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="mb-4 bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
              placeholder="Title"
            />
            <label className="block mb-1 text-sm font-medium text-textSecondary">
              Body
            </label>
            <input
              type="text"
              value={body}
              onChange={(e) => setBody(e.target.value)}
              className="mb-4 bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
              placeholder="Body Post"
            />

            <div className="flex justify-center">
              <button
                type="submit"
                className="mt-6 text-cardBg button-primary text-sm px-4 py-1.5"
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddPosts;
