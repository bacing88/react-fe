import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import CardDetailPost from "../../components/card/CardDetailPost";
import ReservationCard from "../../components/card/ReservationCard";
import {
  getPosts,
  deletePosts,
  postsSelectors,
  updatePosts,
} from "../../feature/postsSlice";

const Marketing = () => {
  const dispatch = useDispatch();
  const posts = useSelector(postsSelectors.selectAll);
  const { isError, isLoading } = useSelector((state) => state.posts);
  const { hidden } = useSelector((state) => state.identitas);
  console.log(hidden);
  useEffect(() => {
    dispatch(getPosts());
  }, [dispatch]);
  const onDelete = useCallback(
    (id) => {
      dispatch(deletePosts(id));
    },
    [dispatch]
  );
  const onUpdate = (id, newObj) => {
    dispatch(updatePosts(id, newObj));
  };

  // const handleAddReservations = () => {
  //   dispatch(addReservation({ name: "testing", id: 3 }));
  //   setDataInput("");
  // };

  if (isError)
    return (
      <div className="p-4 text-md text-danger font-bold">{`${isError.message}`}</div>
    );
  return (
    <div className="flex px-2 card-primary">
      <div className="flex flex-col justify-between w-96 border-r-2 px-4 space-y-2">
        <div className="flex flex-col">
          <p className="text-md font-semibold tracking-widest">Welcome</p>
          <p className="text-textSecondary text-sm -mt-1">List Posts Name</p>
          {isLoading ? (
            <div className="py-4 text-md text-textSecondary font-bold">
              Loading...
            </div>
          ) : (
            <div>
              {posts?.map((name, index) => (
                <div key={index}>
                  <ReservationCard
                    id={name.id}
                    body={name.body}
                    name={name.name}
                    onDelete={onDelete}
                    onUpdate={onUpdate}
                  />
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
      <div className="flex-1 p-4">
        {hidden === "Detail" ? <CardDetailPost /> : <p>Show Data Detail</p>}
      </div>
    </div>
  );
};

export default Marketing;
