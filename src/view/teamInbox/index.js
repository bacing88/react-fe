import React from "react";
import TodoList from "../../components/todo/todoList/TodoList";
import AddTodo from "../../components/todo/addTodo/AddTodo";
const TeamInbox = () => {
  return (
    <div className="card-primary p-4">
      <AddTodo />
      <TodoList />
    </div>
  );
};

export default TeamInbox;
