/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { useParams } from "react-router-dom";
import NavBarReal from "../../components/NavBarReal";
import NavBarMobileReal from "../../components/NavBarMobileReal";
import CardReal from "../../components/realEstateFinder/cardReal";

function detail() {
  const { id } = useParams();
  return (
    <>
      <NavBarReal />
      <CardReal />
      <NavBarMobileReal />
    </>
  );
}

export default detail;
