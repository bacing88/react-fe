import React from "react";
import NavBarReal from "../../components/NavBarReal";
import SliderImage from "../../components/sliderImage";
import NavBarMobileReal from "../../components/NavBarMobileReal";

function Dashboard() {
  return (
    <>
      <NavBarReal />
      <SliderImage />
      <NavBarMobileReal />
    </>
  );
}

export default Dashboard;
