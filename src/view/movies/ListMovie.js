import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getMovies, moviesSelectors } from "../../feature/moviesSlice";
import { useNavigate } from "react-router-dom";

function ListMovie() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const movies = useSelector(moviesSelectors.selectAll);

  useEffect(() => {
    dispatch(getMovies());
  }, [dispatch]);

  return (
    <>
      {movies?.map((item, index) => (
        <div key={index} className="block bg-slate-200">
          <div
            onClick={() => navigate(`/movies/${item.id}`)}
            className="flex justify-center cursor-auto"
          >
            <strong className="relative h-6 bg-black hover:bg-slate-600 px-4 text-xs uppercase leading-6 text-white">
              Detail
            </strong>
          </div>

          <img
            alt="Trainer"
            src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2/${item.poster_path}`}
            className="-mt-6 h-[350px] w-full object-cover sm:h-[450px]"
          />
          <div className="p-2">
            <h3 className="mt-1 text-sm font-bold text-gray-700">
              {item.title}
            </h3>

            <div className="mt-4 flex items-center justify-between font-medium">
              <p>Release Date</p>

              <p className="text-xs uppercase tracking-wide">
                {item.release_date}
              </p>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}

export default ListMovie;
