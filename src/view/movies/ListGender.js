import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getGender, moviesSelectors } from "../../feature/moviesSlice";

function ListGender() {
  const dispatch = useDispatch();
  const movies = useSelector(moviesSelectors.selectAll);
  useEffect(() => {
    dispatch(getGender());
  }, [dispatch]);

  return (
    <>
      {movies?.map((user, index) => (
        <div
          key={index}
          className="p-12 relative h-48 items-end rounded-3xl border-4 border-black bg-white transition hover:bg-yellow-50"
        >
          <div className="p-8 lg:group-hover:absolute lg:group-hover:opacity-0">
            <p className="text-3xl font-bold">{user.id}</p>

            <p className="mt-1 font-medium text-lg">{user.name}</p>
          </div>
        </div>
      ))}
    </>
  );
}

export default ListGender;
