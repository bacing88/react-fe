import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getMovie, moviesSelectors } from "../../feature/moviesSlice";

function DetailMovies() {
  const navigate = useNavigate();
  const { id } = useParams();
  const dispatch = useDispatch();
  const movie = useSelector(moviesSelectors.selectAll);
  console.log("adadadadad", movie);
  useEffect(() => {
    dispatch(getMovie(id));
  }, [dispatch]);
  return (
    <>
      <article className="flex bg-white transition hover:shadow-xl">
        <div className="rotate-180 p-2 [writing-mode:_vertical-lr]">
          <time
            datetime="2022-10-10"
            className="flex items-center justify-between gap-4 text-xs font-bold uppercase text-gray-900"
          >
            <span>vote_average</span>
            <span className="w-px flex-1 bg-gray-900/10"></span>
            <span>{movie[0].vote_average}</span>
          </time>
        </div>

        <div className="hidden sm:block sm:basis-56">
          <img
            alt="Guitar"
            src={`https://image.tmdb.org/t/p/w600_and_h900_bestv2/${movie[0].poster_path}`}
            className="aspect-square h-full w-full object-cover"
          />
        </div>

        <div className="flex flex-1 flex-col justify-between">
          <div className="border-l border-gray-900/10 p-4 sm:border-l-transparent sm:p-6">
            <div>
              <h3 className="font-bold uppercase text-gray-900">
                {movie[0].title}
              </h3>
            </div>

            <p className="mt-2 text-sm leading-relaxed text-gray-700 line-clamp-3">
              {movie[0].overview}
            </p>
          </div>
          <div className="px-6">
            <h3 className="mt-1 text-sm font-bold text-gray-700">
              {movie[0].homepage}
            </h3>

            <div className="mt-2 flex items-center justify-between font-medium">
              <p>Release Date</p>

              <p className="text-xs uppercase tracking-wide">
                {movie[0].release_date}
              </p>
            </div>
            <div className="mt-2 flex items-center justify-between font-medium">
              <p>Tagline</p>

              <p className="text-xs uppercase tracking-wide">
                {movie[0].tagline}
              </p>
            </div>
          </div>

          <div className="sm:flex sm:items-end sm:justify-end">
            <div
              onClick={() => navigate("/movies/list-movie")}
              className="block bg-yellow-300 px-5 py-3 text-center text-xs font-bold uppercase text-gray-900 transition hover:bg-yellow-400"
            >
              Back List
            </div>
          </div>
        </div>
      </article>
    </>
  );
}

export default DetailMovies;
