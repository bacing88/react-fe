import React from "react";
import { NavLink, Outlet } from "react-router-dom";
function Movies() {
  return (
    <>
      <nav className="flex mt-6 bg-cardBg rounded-lg">
        <NavLink
          to="/movies/list-gender"
          className={({ isActive }) =>
            isActive
              ? "flex items-center px-4 py-2 button-primary"
              : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          }
        >
          <span className="text-sm font-medium"> List Genre </span>
        </NavLink>
        <NavLink
          to="/movies/list-movie"
          className={({ isActive }) =>
            isActive
              ? "flex items-center px-4 py-2 button-primary"
              : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          }
        >
          <span className="text-sm font-medium"> List Movies </span>
        </NavLink>
      </nav>
      <div className="grid lg:grid-cols-4 md:grid-cols-2 grid-cols-1 gap-2 pt-6">
        <Outlet />
      </div>
    </>
  );
}

export default Movies;
