import React from "react";

const WhatsappShop = () => {
  return (
    <>
      <h1 className="text-3xl font-bold text-primary-200">WhatsappShop</h1>
    </>
  );
};

export default WhatsappShop;
