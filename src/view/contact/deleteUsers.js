import { database } from "../../firebaseConfig";
import { doc, deleteDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
export default function DeleteUsers({ todo }) {
  const navigate = useNavigate();
  const deleteDocument = (id) => {
    let fieldToEdit = doc(database, "Crud Users", id);
    deleteDoc(fieldToEdit)
      .then(() => {
        navigate(0);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div>
      <button
        className="py-1 px-2 text-sm bg-red-200 hover:bg-red-300 text-red-500 hover:text-red-600 rounded-md"
        onClick={() => {
          deleteDocument(todo.split("/")[6]);
        }}
      >
        Delete
      </button>
    </div>
  );
}
