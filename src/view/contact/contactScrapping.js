import React from "react";

const ContactScrapping = () => {
  return (
    <>
      <div className="flex gap-2">
        <div className="card-primary px-6 py-4 w-1/2">
          <p className="text-lg text-textPrimary font-semibold">
            Contact Scrapping
          </p>
          <form>
            <div className="m-6">
              <label className="block mb-1 text-sm font-medium text-textSecondary">
                Server
              </label>
              <select
                id="countries"
                className="mb-4 appearance-none bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
              >
                <option selected>Please Select Divice</option>
                <option value="US">United States</option>
                <option value="CA">Canada</option>
                <option value="FR">France</option>
                <option value="DE">Germany</option>
              </select>
              <label className="block mb-1 text-sm font-medium text-textSecondary">
                Start Number
              </label>
              <input
                type="text"
                id="servers"
                className="mb-4 bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
                placeholder="Input 4 Number"
                required
              />
              <label className="block mb-1 text-sm font-medium text-textSecondary">
                Random Count
              </label>
              <input
                type="text"
                id="servers"
                className="mb-4 bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
                placeholder="Input Number"
                required
              />
              <label className="block mb-1 text-sm font-medium text-textSecondary">
                Quantity Looping
              </label>
              <input
                type="text"
                id="servers"
                className="bg-gray-50 border border-textSecondary text-textPrimary text-sm rounded-md block w-full px-3 py-1 outline-none"
                placeholder="Input Quantity"
                required
              />
              <div className="flex justify-center">
                <button
                  type="submit"
                  className="mt-6 text-cardBg button-primary text-sm px-4 py-1.5"
                >
                  Submit
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className="card-primary px-6 py-4 w-1/2">
          <p className="text-lg text-textPrimary font-semibold">Result</p>
        </div>
      </div>
    </>
  );
};

export default ContactScrapping;
