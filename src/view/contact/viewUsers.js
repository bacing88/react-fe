import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import moment from "moment";

export default function ViewUsers({ todo }) {
  const [isOpen, setIsOpen] = useState(false);
  function closeModal() {
    setIsOpen(false);
  }
  function openModal() {
    setIsOpen(true);
  }
  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="py-1 px-2 text-sm bg-green-200 hover:bg-green-300 text-green-600 hover:text-green-700 rounded-md"
      >
        View
      </button>

      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-bold leading-6 text-gray-800"
                  >
                    Detail User
                  </Dialog.Title>

                  <div className="mt-5">
                    <ul className="px-2 space-y-1 text-black">
                      <li>
                        <span className="font-bold pr-2">Nama :</span>
                        {todo.fields.nama.stringValue}
                      </li>
                      <li>
                        <span className="font-bold pr-2">Alamat :</span>
                        {todo.fields.alamat.stringValue}
                      </li>
                      <li>
                        <span className="font-bold pr-2">Gander :</span>
                        {todo.fields.gander.stringValue
                          .charAt(0)
                          .toUpperCase() +
                          todo.fields.gander.stringValue.slice(1)}
                      </li>
                      <li>
                        <span className="font-bold pr-2">Tanggal Lahir :</span>
                        {moment
                          .unix(todo.fields.birth.stringValue)
                          .format("DD MMMM YYYY")}
                      </li>
                      <li>
                        <span className="font-bold pr-2">Tanggal Input :</span>
                        {moment(todo.createTime).format("DD MMMM YYYY hh:mm")}
                      </li>
                    </ul>
                    <p className="flex justify-center py-4 font-bold">
                      {todo.name.split("/")[6]}
                    </p>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
