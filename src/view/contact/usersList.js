import React from "react";
import { UsersListTable } from "../../components/table/UsersListTable";
import AddUsers from "./addUsers";

const UsersList = () => {
  return (
    <>
      <div className="card-primary px-6 py-4 w-full space-y-4">
        <div className="flex justify-between">
          <p className="text-lg text-textPrimary font-semibold">Users List</p>
          <AddUsers />
        </div>
        <UsersListTable />
      </div>
    </>
  );
};

export default UsersList;
