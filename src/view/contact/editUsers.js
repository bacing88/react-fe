import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import { PlusCircleIcon } from "@heroicons/react/24/outline";
import { useForm } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";
import { database } from "../../firebaseConfig";
import { doc, updateDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import moment from "moment";

export default function AddUsers({ todo }) {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  let fieldToEdit = doc(database, "Crud Users", todo.name.split("/")[6]);
  function closeModal() {
    setIsOpen(false);
  }
  function openModal() {
    setIsOpen(true);
  }
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();
  const updateData = (data) => {
    updateDoc(fieldToEdit, {
      nama: data.nama,
      alamat: data.alamat,
      gander: data.gander,
      birth: (Date.parse(data.birth) / 1000).toString(),
    })
      .then(() => {
        closeModal();
        navigate(0);
      })
      .catch((err) => {
        console.error(err);
      });
  };
  const onSubmit = (data) => updateData(data);

  console.log(errors);

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="py-1 px-2 text-sm bg-blue-200 hover:bg-blue-300 text-blue-600 hover:text-blue-700 rounded-md"
      >
        Edit
      </button>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-bold leading-6 text-gray-900"
                  >
                    Edit User
                  </Dialog.Title>
                  <div className="mt-5">
                    <form onSubmit={handleSubmit(onSubmit)}>
                      <label className="text-gray-600">
                        Nama : <span className="text-red-500">*</span>
                      </label>
                      <input
                        className="input-primary"
                        type="text"
                        defaultValue={todo.fields.nama.stringValue}
                        setValue={todo.fields.nama.stringValue}
                        {...register("nama", { required: "This is required." })}
                      />
                      <ErrorMessage
                        errors={errors}
                        name="nama"
                        render={({ message }) => (
                          <p className="mb-1 text-normal text-red-500">
                            {message}
                          </p>
                        )}
                      />
                      <label className="text-gray-600">
                        Alamat : <span className="text-red-500">*</span>
                      </label>
                      <input
                        className="input-primary"
                        type="text"
                        defaultValue={todo.fields.alamat.stringValue}
                        setValue={todo.fields.alamat.stringValue}
                        {...register("alamat", {
                          required: "This is required.",
                        })}
                      />
                      <ErrorMessage
                        errors={errors}
                        name="alamat"
                        render={({ message }) => (
                          <p className="mb-1 text-normal text-red-500">
                            {message}
                          </p>
                        )}
                      />
                      <div className="flex mt-4 mb-3">
                        <div className="pr-4">
                          <label className="text-gray-600">P / W : </label>
                        </div>
                        <div className="space-x-2">
                          <input
                            {...register("gander", { required: true })}
                            type="radio"
                            value={"pria"}
                          />
                          <label className="peer-checked/draft:text-sky-500">
                            Pria
                          </label>
                          <input
                            className="accent-pink-500"
                            {...register("gander", { required: true })}
                            type="radio"
                            value={"wanita"}
                          />
                          <label className="peer-checked/draft:text-sky-500">
                            Wanita
                          </label>
                        </div>
                      </div>
                      <label className="text-gray-600">
                        Tanggal Lahir :{" "}
                        <span className="text-gray-500">
                          {moment
                            .unix(todo.fields.birth.stringValue)
                            .format("DD MMMM YYYY")}
                        </span>
                      </label>
                      <input
                        className="input-primary"
                        type="datetime-local"
                        defaultValue={todo.createTime}
                        {...register("birth", {})}
                      />

                      <input
                        type="submit"
                        className="mt-4 w-full bg-green-400 hover:bg-green-600 text-green-100 border py-2 px-6 font-semibold text-md rounded"
                      />
                    </form>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
