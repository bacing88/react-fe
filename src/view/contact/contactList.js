import React from "react";
import { PlusCircleIcon } from "@heroicons/react/24/outline";
import { ContactListTable } from "../../components/table/ContactListTable";

const ContactList = () => {
  return (
    <>
      <div className="card-primary px-6 py-4 w-full space-y-4">
        <div className="flex justify-between">
          <p className="text-lg text-textPrimary font-semibold">Contact List</p>
          <div className="flex space-x-1">
            <button className="flex items-center space-x-2 button-primary">
              <span className="text-sm font-medium"> Export Excel</span>
            </button>
            <button className="flex items-center space-x-2 button-primary">
              <span className="text-sm font-medium"> Delete </span>
            </button>
            <button className="flex items-center space-x-2 button-primary">
              <PlusCircleIcon className="h-5 w-5" />
              <span className="mr-3 text-sm font-medium"> Add Contact </span>
            </button>
          </div>
        </div>

        <div className="relative w-full">
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
            <svg
              aria-hidden="true"
              className="w-5 h-5 text-textSecondary"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </div>
          <input
            type="text"
            id="simple-search"
            className="bg-cardBg border border-textSecondary text-textPrimary text-sm rounded-lg w-full pl-10 px-2 py-1.5 outline-none"
            placeholder="Search"
            required
          />
        </div>
        <ContactListTable />
      </div>
    </>
  );
};

export default ContactList;
