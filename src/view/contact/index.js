import React from "react";
import { NavLink, Outlet } from "react-router-dom";
const Contact = () => {
  return (
    <>
      <nav className="flex mt-6 bg-cardBg rounded-lg select-none">
        <NavLink
          to="/contact/usersList"
          className={({ isActive }) =>
            isActive
              ? "flex items-center px-4 py-2 button-primary"
              : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          }
        >
          <span className="text-sm font-medium"> Users List </span>
        </NavLink>
        <NavLink
          to="/contact/contactList"
          className={({ isActive }) =>
            isActive
              ? "flex items-center px-4 py-2 button-primary"
              : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          }
        >
          <span className="text-sm font-medium"> Contact List </span>
        </NavLink>
        <NavLink
          to="/contact/contactScrapping"
          className={({ isActive }) =>
            isActive
              ? "flex items-center px-4 py-2 button-primary"
              : "flex items-center px-4 py-2 text-secondary-100 rounded-lg hover:bg-gray-50 hover:text-primary-500"
          }
        >
          <span className="text-sm font-medium"> Contact Scrapping </span>
        </NavLink>
      </nav>

      <Outlet />
    </>
  );
};

export default Contact;
