import React from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import { tomorrowNightEighties } from "react-syntax-highlighter/dist/esm/styles/hljs";

const Report = () => {
  const codeString = `//Number To Array
const myInt = 123456
const myFunc = num => Number(num); 
const intArr = Array.from(String(myInt), myFunc);
console.log(intArr)  `;

  return (
    <>
      <div className="flex flex-col justify-start mb-8">
        <div className="w-full bg-[#2D2D2D]">
          <div className="flex justify-between items-center py-2 px-4 ">
            <div id="header-buttons" className="flex">
              <div className="rounded-full w-3 h-3 bg-red-500 mr-2"></div>
              <div className="rounded-full w-3 h-3 bg-yellow-500 mr-2"></div>
              <div className="rounded-full w-3 h-3 bg-green-500"></div>
            </div>
            <p className="text-xs text-white">JavaScript</p>
          </div>
        </div>

        <SyntaxHighlighter
          language="javascript"
          style={tomorrowNightEighties}
          showLineNumbers={true}
        >
          {codeString}
        </SyntaxHighlighter>
      </div>
      <h1 className="text-xl font-bold text-primary-500">Primary-500</h1>
      <h1 className="text-xl font-bold text-primary-400">Primary-400</h1>
      <h1 className="text-xl font-bold text-primary-300">Primary-300</h1>
      <h1 className="text-xl font-bold text-primary-200">Primary-200</h1>
      <h1 className="text-xl font-bold text-primary-100">Primary-100</h1>
      <h1 className="text-xl font-bold text-secondary-200">Secondary-200</h1>
      <h1 className="text-xl font-bold text-secondary-100">Secondary-100</h1>
      <h1 className="text-lg font-semibold text-danger">Danger!</h1>
      <h1 className="text-2xl font-bold text-textPrimary">Text Primary</h1>
      <h1 className="text-lg text-textSecondary">Text Secondary</h1>
      <button className="w-16 mb-1 rounded-sm bg-gradient-to-br from-multiColorBtn-200 to-multiColorBtn-100 px-1 text-white">
        button
      </button>
      <div className="card-secondary w-56">
        Upgrade to PRO to get access all Features!
      </div>
      <div className="card-primary mb-1 p-4">cardBg</div>
      <div className="card-primary p-4">bgLayout</div>
    </>
  );
};

export default Report;
