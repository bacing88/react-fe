import { configureStore } from "@reduxjs/toolkit";
import identitasReducer from "../feature/identitasSlice";
import usersReducer from "../feature/usersSlice";
import reservationsReducer from "../feature/reservationSlice";
import customerReducer from "../feature/customerSlice";
import postsReducer from "../feature/postsSlice";
import todoReducer from "../feature/todoSlice";
import moviesReducer from "../feature/moviesSlice";

export const store = configureStore({
  reducer: {
    movies: moviesReducer,
    identitas: identitasReducer,
    users: usersReducer,
    reservations: reservationsReducer,
    customer: customerReducer,
    posts: postsReducer,
    todos: todoReducer,
  },
});
