import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCDr9j9OwHd62Y_hzZnmYW__ZlVWg2We3g",
  authDomain: "testing-crud-843a3.firebaseapp.com",
  projectId: "testing-crud-843a3",
  storageBucket: "testing-crud-843a3.appspot.com",
  messagingSenderId: "331654422529",
  appId: "1:331654422529:web:161736af783253bb03a000",
};
export const app = initializeApp(firebaseConfig);
export const database = getFirestore(app);
